import React from 'react';
import './Section.css'

import avatar from '../../images/pc.jpg'

const Section = () => {
    return (
        <div className="about-me" id="about">
        <h2 className="about-me__title">About me</h2>
        <div className="Section">
            <div className="personal__photo__container">
                <img src={avatar} className="personal__photo" alt="avatar" />
            </div>
            <div className="personal__info__container" >
                <h3>Fernando Sandoval</h3>
                <p>
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                    Necessitatibus provident deleniti inventore nesciunt consequatur.
                    Hic velit nisi corrupti pariatur soluta nemo, iure est cumque inventore
                    maiores ipsam autem. Iusto, illo!
                </p>
            </div>

        </div></div>
    );
}

export default Section;