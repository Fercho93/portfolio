import React from 'react';
import './Parallax.css'

const Parallax = () => {
    return (
        <div className="Parallax">
            <h3 className="parallax__title">Projects</h3>
        </div>
    );
}

export default Parallax;