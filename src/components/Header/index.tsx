import React, { useRef } from 'react';
import './Header.css'

import {FaBars} from 'react-icons/fa'

const Header = () => {
    const navRef = useRef(null)

    const showNav = () => {
        const nav: HTMLElement = navRef.current
        nav.classList.toggle('active')
    }

    return (
        <div className="Header" >

            {/* <img className="svg" src={svg} alt="fasda" /> */}
            <nav className="Header__nav"  >
                <h2 className="nav__logo">Fer Dev</h2>
                <ul className="nav__links" ref={navRef} >
                    <a href="#projects" onClick={showNav}>My Work</a>
                    <a href="#about" onClick={showNav}>Who am I?</a>
                    <a href="/" onClick={showNav}>Contact me</a>
                </ul>
                <div className="Header__menu" onClick={showNav}>
                    <FaBars/>
                </div>
            </nav>

            <div className="personal__info-container">
                <div className="personal__info">
                    <div>
                        <h3 className="personal__info-name">Web development</h3>
                        <p className="personal__info-job">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            <br />
                            Quas maxime assumenda ducimus sunt
                        </p>
                        <button className="contact-btn">Contact me</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Header;