import React, { useState } from 'react'
import Slider from "react-slick";
import './Projects.css'

import { FaArrowRight, FaArrowLeft } from 'react-icons/fa'

const projects = [
    { 
        title: 'project 1', 
        img: 'https://miro.medium.com/max/3920/1*oZqGznbYXJfBlvGp5gQlYQ.jpeg',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi inventore ducimus enim consequuntur' },
    { 
        title: 'project 2', 
        img: 'https://cdn-media-1.freecodecamp.org/ghost/2019/05/laravel2-1.png',
        description: 'quam sed commodi provident, nesciunt asperiores voluptatem deserunt consequatur debitis facilis, recusandae' },
    { 
        title: 'project 3', 
        img: 'https://miro.medium.com/max/3840/0*oZLL-N4dGNlBe4Oh.png',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi inventore ducimus enim consequuntur' },
    { 
        title: 'project 4', 
        img: 'https://www.solucionex.com/sites/default/files/posts/imagen/phplogo.png',
        description: 'quam sed commodi provident, nesciunt asperiores voluptatem deserunt consequatur debitis facilis, recusandae' },
    // { title: 'project', description: 'quam sed commodi provident, nesciunt asperiores voluptatem deserunt consequatur debitis facilis, recusandae' },
    // { title: 'project', description: 'quam sed commodi provident, nesciunt asperiores voluptatem deserunt consequatur debitis facilis, recusandae' },
    // { title: 'project', description: 'quam sed commodi provident, nesciunt asperiores voluptatem deserunt consequatur debitis facilis, recusandae' },
    // { title: 'project', description: 'quam sed commodi provident, nesciunt asperiores voluptatem deserunt consequatur debitis facilis, recusandae' },
]

// const colors = ['primary', 'secondary', 'dark', 'light']

export default function Projects() {

    const [projectIndex, setProjectIndex] = useState(0)

    const settings = {
        infinite: true,
        speed: 200,
        slidesToShow: 3,
        centerMode: true,
        centerPadding: 0,
        nextArrow: <ArrowNext />,
        prevArrow: <ArrowPrev />,
        beforeChange: (current, next) => setProjectIndex(next)
    };
    return (
        <div className="Slider-container">
            {/* <h2>Center Mode</h2> */}
            <Slider {...settings}>
                {projects.map((project, index) => (
                    <div className={index === projectIndex ? 'slide activeSlide' : 'slide'} key={index}>
                        <div
                            className="project">
                            <h3 className="slide__title"> {project.title} </h3>
                            <img src={project.img} alt={project.title}/>
                        </div>
                    </div>
                ))}
            </Slider>
        </div>
    );
}

function ArrowPrev(props) {
    const { onClick } = props
    return (
        <div
            className='arrow prev'
            onClick={onClick}
        >
            <FaArrowLeft />
        </div>
    );
}
function ArrowNext(props) {
    const { onClick } = props
    return (
        <div
            className='arrow next'
            onClick={onClick}
        >
            <FaArrowRight />
        </div>
    );
}