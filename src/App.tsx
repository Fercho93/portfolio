import React from 'react';
import './App.css';
import Footer from './components/Footer';
import Header from './components/Header';
import Parallax from './components/Parallax';
import Projects from './components/Projects';
import Section from './components/Section';

function App() {

  return (
    <div className="App">
     <main className="main">
      <Header />
      <Section />
        <div id="projects">
          <Parallax />
          <Projects />
        </div>
      <Footer />
     </main>
    </div>
  );
}

export default App;
